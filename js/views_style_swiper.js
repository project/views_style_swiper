(function(Drupal, Swiper){
  Drupal.behaviors.views_style_swiper_style = {
    attach: function(context, drupalSettings) {
      // Do nothing if we have no settings to work on
      if(!('viewsStyleSwiper' in drupalSettings)) return;

      for(var dom_id in drupalSettings.viewsStyleSwiper) {
        var elements = context.getElementsByClassName(dom_id);

        // Do nohing if we cant find any interesting elements in the context
        if(!elements.length) continue;

        // initialize the swiper
        for(var i=0; i<elements.length; i++) {
          if(!elements[i].swiper) {
            new Swiper(elements[i], drupalSettings.viewsStyleSwiper[dom_id]);
          }
        }
      }
    }
  };
})(Drupal, Swiper);
