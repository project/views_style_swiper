<?php

namespace Drupal\views_style_swiper\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Unformatted style plugin to render rows one after another with no
 * decorations.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_style_swiper_style",
 *   title = @Translation("Swiper"),
 *   help = @Translation("Displays rows in a swiper."),
 *   theme = "views_view_views_style_swiper_style",
 *   display_types = {"normal"}
 * )
 */
class ViewsStyleSwiperStyle extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * The swiper settings.
   *
   * @var array
   */
   protected $swiperSettings = [];

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['swiper_settings'] = ['default' => []];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['swiper_settings'] = [
      '#title' => $this->t('Swipper settings'),
      '#description' => $this->t('Swiper settings in YAML format'),
      '#type' => 'textarea',
      '#default_value' => $this->options['swiper_settings'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    parent::validateOptionsForm($form, $form_state);
    $style_options = $form_state->getValue('style_options');
    if(!empty($style_options['swiper_settings'])) {
      try {
        Yaml::parse($style_options['swiper_settings']);
      } catch (ParseException $exception) {
        $form_state->setErrorByName('swiper_settings', $this->t('Swiper settings YAML is not valid: %msg', ['%msg' => $exception->getMessage()]));
      }
    }
  }
}
