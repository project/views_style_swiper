<?php

namespace Drupal\views_style_swiper\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\Yaml\Yaml;
use Drupal\Component\Utility\Html;

/**   
 * Plugin implementation of the 'swiper entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "views_style_swiper_entity_reference_entity_formatter",
 *   label = @Translation("Rendered entity in swiper"),
 *   description = @Translation("Display the referenced entities rendered by entity_view() in a swiper."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ViewsStyleSwiperEntityReferenceEntityFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'swiper_settings' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['swiper_settings'] = [
      '#title' => $this->t('Swipper settings'),
      '#description' => $this->t('Swiper settings in YAML format'),
      '#type' => 'textarea',
      '#default_value' => $this->getSetting('swiper_settings'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewelements($items, $langcode);
    if (!empty($elements)) {
      return [
        '#theme' => 'views_style_swiper_entity_formatter',
        '#elements' => $elements,
        '#dom_id' => Html::getUniqueId('js-views_style_swiper_entity_formatter'),
        '#swiper_settings' => Yaml::parse($this->getSetting('swiper_settings')),
      ];
    }
    return [];
  }

}
